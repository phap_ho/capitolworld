﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectatorAnamation : MonoBehaviour
{
    private void Awake()
    {
        var anim = GetComponentsInChildren<Animator>();

        foreach(Animator animator in anim)
        {
            if (UserInfoManager.Instance.userInfo.club == Club.BlueTeam)
            {
                animator.SetTrigger("Blue");
            }
            else
            {
                animator.SetTrigger("Red");

            }
        }


    }
}
