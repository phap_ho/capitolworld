﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : Mascot
{
    private float animTime;
    private bool endOfKickAnim = false;
    [SerializeField] Transform idlePos;
    private bool detectedEndAnim;
    private float speedRotate;
    private PlayerDataBinding playerDataBinding;

    [SerializeField] AudioSource audioSource;
    public override void OnAwake()
    {
        base.OnAwake();
        animTime = 0;
        endOfKickAnim = false;
        detectedEndAnim = false;
        speedRotate = 0;
        playerDataBinding = player.GetComponent<PlayerController>().playerDataBinding;
        audioSource = GetComponent<AudioSource>();
    }
    public override void IdleState()
    {
        gameObject.transform.position = idlePos.position;
        if (playerDataBinding.Speed == 0.5)
        {
            speedRotate = speedRotate < 4 ? speedRotate + 0.1f : 4;
        }
        else
        {
            speedRotate = speedRotate > 0 ? speedRotate - 0.1f : 0;
        }
        Rotate(speedRotate);
    }

    public override void AttackState(Vector3 aim)
    {
        base.AttackState(aim);
        base.AttackState(aim);
        if (!detectedEndAnim)
        {
            StartCoroutine(EndOfAnim());
            detectedEndAnim = true;
        }
        if (endOfKickAnim)
        {
            StopCoroutine(EndOfAnim());
            animTime += Time.deltaTime;
            animTime %= 2f;
            Vector3 aimPos = aim;
            transform.position = MathParabola.Parabola(idlePos.position, aimPos, aimPos.y, animTime / 2f);
        }
    }

    public override void AttackState(Flag flag)
    {
        base.AttackState(flag);
        if (!detectedEndAnim)
        {
            StartCoroutine(EndOfAnim());
            detectedEndAnim = true;
        }
        if (endOfKickAnim)
        {
            StopCoroutine(EndOfAnim());
            animTime += Time.deltaTime;
            animTime %= 2f;
            Vector3 aimPos = flag.transform.position;
            if (flag.flagPositionType == FlagPositionType.WindowFlag)
                transform.position = MathParabola.Parabola(idlePos.position, aimPos, aimPos.y, animTime / 2f);
            else
                transform.position = MathParabola.Parabola(idlePos.position, aimPos, aimPos.y, animTime);
        }
    }
    IEnumerator EndOfAnim()
    {
        gameObject.transform.position = idlePos.position;
        yield return new WaitForSeconds(0.5f);
        endOfKickAnim = true;
        audioSource.Play();

    }
    public override void OnUpdate()
    {
        if(mascotState == MascotState.AttackWrongObj)
        {
            if(Vector3.Distance(transform.position, aimFlag) < 10)
            {
                mascotState = MascotState.Idle;
                animTime = 0;
                endOfKickAnim = false;
                detectedEndAnim = false;
            }
        }
    }
    public override void OnAttackWrongObject(Vector3 pos)
    {
        base.OnAttackWrongObject(pos);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Contains("Flag"))
        {
            if (other.gameObject.GetComponent<Flag>().canAttack)
            {
                //Debug.LogError("Change to idle state");
                mascotState = MascotState.Idle;
                animTime = 0;
                endOfKickAnim = false;
                detectedEndAnim = false;
            }
        }
    }
    private void Rotate(float speed)
    {
        transform.Rotate(idlePos.position - player.transform.position, speed);
    }
}
