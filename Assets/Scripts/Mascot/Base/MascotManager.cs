﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum MascotType
{
    ObjectThrow,
    Eagle,
    Animal,
    SoccerBall,
}
public class MascotManager : MonoBehaviour
{

    [SerializeField] List<Mascot> mascots;
    public Dictionary<Club, Mascot> dicMascot;
    private void Awake()
    {
        dicMascot = new Dictionary<Club, Mascot>();
        foreach(Mascot m in mascots)
        {
            dicMascot.Add(m.mascotOfClub, m);
        }
        //dicMascot.Add(Club.Monchengladbach, dicMascot[Club.Hannover]);
        dicMascot.Add(Club.BlueTeam, dicMascot[Club.RedTeam]);
        dicMascot[UserInfoManager.Instance.userInfo.club].gameObject.SetActive(true);
    }
    private void Start()
    {
    }
}
