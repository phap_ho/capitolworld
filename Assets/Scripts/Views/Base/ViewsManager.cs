﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewsManager : Singleton<ViewsManager>
{
    [SerializeField] List<Views> views;
    public Dictionary<ViewType, Views> dicViews;
    public ViewType previousView;
    public Views currentView;
    private void Awake()
    {
        dicViews = new Dictionary<ViewType, Views>();
        foreach (Views e in views)
        {
            var cv = Instantiate(e);
            cv.transform.SetParent(gameObject.transform);
            cv.gameObject.SetActive(false);
            dicViews.Add(e.viewType, cv);
        }
        currentView = dicViews[ViewType.EmptyView];
        currentView.gameObject.SetActive(true);
    }
    public void ChangeView(ViewType newView)
    {
        previousView = currentView.viewType;
        currentView.gameObject.SetActive(false);
        currentView = dicViews[newView];
        currentView.gameObject.SetActive(true);
    }
    public void ChangeView(ViewType preView, ViewType newView)
    {
        previousView = preView;
        currentView.gameObject.SetActive(false);
        currentView = dicViews[newView];
        currentView.gameObject.SetActive(true);
    }
    public void LoadSceneByName(string name)
    {
        currentView.gameObject.SetActive(false);
        currentView = dicViews[ViewType.LoadingView];
        currentView.gameObject.SetActive(true);
        LoadingView loadingView = currentView as LoadingView;
        loadingView.LoadSceneByName(name);
    }
    public void NoView()
    {
        currentView.gameObject.SetActive(false);
    }
}
