﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SelectRoundView : Views
{
    [SerializeField] List<Button> buttons;
    public void ButtonInteract(bool canInteract)
    {
        foreach (Button button in buttons)
        {
            button.interactable = canInteract;
        }
    }
    public override void SetUp()
    {
        base.SetUp();
        ButtonInteract(true);
    }
    public void GotoSetupView()
    {
        ViewsManager.Instance.ChangeView(ViewType.SetupView);
    }
    public void GotoIAPView()
    {
        ViewsManager.Instance.ChangeView(ViewType.IAPView);
    }
    public void GotoTheCapinetView()
    {
        TheCapinetView.previousView = ViewType.SelectRoundView;
        ViewsManager.Instance.ChangeView(ViewType.TheCapinetView);
    }
    public void GotoLeaderBoardView()
    {
        ViewsManager.Instance.ChangeView(ViewType.LeaderBoardView);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void ShowNotice(Image notice)
    {
        ButtonInteract(false);
        notice.gameObject.SetActive(true);
    }
    public void OffNotice(Image notice)
    {
        notice.gameObject.SetActive(false);
        ButtonInteract(true);
    }
}


