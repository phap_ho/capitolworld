﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignUpView : Views
{
    [SerializeField] InputField email;
    [SerializeField] InputField password;
    [SerializeField] Image Success;
    [SerializeField] Image Fail;

    private bool signupSuccess;
    private bool signupFail;

    public override void SetUp()
    {
        Success.gameObject.SetActive(false);
        Fail.gameObject.SetActive(false);
    }
    public void SignUp()
    {
        if (PlatformManager.inst.platformType == PlatformType.UWP)
            AuthRESTAPI.SignUp(email.text, password.text);
        else Auth.SignUp(email.text, password.text);
    }
    public void ChangeToLogin()
    {
        ViewsManager.Instance.ChangeView(ViewType.LoginView);

    }
    public void ResetPassword()
    {
        ViewsManager.Instance.ChangeView(ViewType.ForgotPassView);

    }
    public override void OnAwake()
    {
        Auth.signUp += (ob) =>
        {
            if (ob) signupSuccess = true;
            else
                signupFail = true;
        };
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        if (signupSuccess)
        {
            StartCoroutine(SignUpSuccess());
        }
        if (signupFail)
        {
            Fail.gameObject.SetActive(true);
        }
    }
    IEnumerator SignUpSuccess()
    {
        signupSuccess = false;
        Success.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        ViewsManager.Instance.ChangeView(ViewType.LoginView);
    }
}

