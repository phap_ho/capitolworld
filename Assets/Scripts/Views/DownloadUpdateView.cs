﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownloadUpdateView : Views
{
    [SerializeField] Text percentLoaded;
    [SerializeField] Image progressBar;
    [SerializeField] List<Image> backgroundOnboarding;
    [SerializeField] Text notice;
    public override void SetUp()
    {
        progressBar.fillAmount = 0f;
        percentLoaded.text = (progressBar.fillAmount * 100).ToString("0") + "%";
        base.SetUp();
        backgroundOnboarding[Random.Range(0, backgroundOnboarding.Count)].gameObject.SetActive(true);
    }
    private void OnDisable()
    {
        foreach (Image image in backgroundOnboarding)
        {
            image.gameObject.SetActive(false);
        }
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        //progressBar.fillAmount = LoadFromStorageRemote.Instance.uwr.downloadProgress;
        //percentLoaded.text = ((progressBar.fillAmount)*100).ToString("0") + "%";

        //if (LoadFromStorageRemote.Instance.uwr.downloadProgress == 1)
        //    notice.text = "Installing resources...";
        //else
        //    notice.text = "Downloading resources...";
    }
}
