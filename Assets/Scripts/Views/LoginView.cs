﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class LoginView : Views
{
    [SerializeField] InputField email;
    [SerializeField] InputField password;
    [SerializeField] GameObject success;
    [SerializeField] GameObject fail;

    private bool loginSucess;
    private bool loginFail;
    private string userId;

    [SerializeField] Image exit;

    public override void OnStart()
    {
        base.OnStart();
        Auth.login += OnLogin;
    }

    public override void SetUp()
    {
        base.SetUp();
        success.SetActive(false);
        fail.SetActive(false);
        exit.gameObject.SetActive(false);
    }

    public void OnLogin(bool ob, string userId)
    {
        if (ob)
        {
            this.userId = userId;
            loginSucess = true;
            //Debug.LogError("Login Action");
        }
        else
        {
            loginFail = true;
        }
    }

    public void Login()
    {
        if (PlatformManager.inst.platformType == PlatformType.UWP)
            AuthRESTAPI.SignIn(email.text, password.text);
        else
            Auth.SignIn(email.text, password.text);
    }
    IEnumerator MoveToSetup()
    {
        loginSucess = false;
        UserInfoManager.Instance.GetData(userId);
        yield return new WaitForSeconds(1);
        //Debug.LogError("Move to setup");
        if (UserInfoManager.Instance.userInfo == null)
        {
            UserInfoManager.Instance.userInfo = new UserInfo
            {
                userID = "",
                name = "",
                coinsNum = 0,
                club = Club.RedTeam,
            };
            UserInfoManager.Instance.userInfo.userID = userId;
            UserInfoManager.Instance.SaveData();
        }
        else
        {
            //UserInfoManager.Instance.GetData(userId);
        }
        success.SetActive(true);
        yield return new WaitForSeconds(3);
        ViewsManager.Instance.LoadSceneByName("Setup");
    }

    public void SignUp()
    {
        ViewsManager.Instance.ChangeView(ViewType.SignUpView);
    }
    public void ForgorPassWord()
    {
        ViewsManager.Instance.ChangeView(ViewType.ForgotPassView);
    }
    public override void OnUpdate()
    {
        if (loginSucess)
        {
            StartCoroutine(MoveToSetup());
        }
        if (loginFail)
        {
            loginFail = false;
            fail.SetActive(true);
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            ShowNotice(exit);
        }
    }

    public void ShowNotice(Image notice)
    {
        notice.gameObject.SetActive(true);
    }
    public void OffNotice(Image notice)
    {
        notice.gameObject.SetActive(false);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}

