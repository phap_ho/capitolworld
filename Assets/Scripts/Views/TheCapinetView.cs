﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System;
using System.Globalization;
using UnityEngine.UI;
using UnityEditor;
public class TheCapinetView : Views
{
    private readonly DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
    [SerializeField] List<Text> names;
    [SerializeField] List<InputField> messages;
    private List<string> mes = new List<string>();
    private bool displayMessage;

    [SerializeField] List<Text> names1;
    [SerializeField] List<InputField> messages1;
    private List<string> mes1 = new List<string>();
    private bool displayMessage1;

    [SerializeField] List<GameObject> tables;
    [SerializeField] List<Image> buttons;
    [SerializeField] List<Text> titles;
    public static ViewType previousView;

    [SerializeField] Button exit;
    [SerializeField] Button save;
    
    public Color color = new Color();
    public Color color2 = new Color();

    [SerializeField] List<Image> cabinetBackground;
    [SerializeField] List<Image> senatBackground;
    public override void OnAwake()
    {
        base.OnAwake();
        ColorUtility.TryParseHtmlString("#0000A2", out color);
        ColorUtility.TryParseHtmlString("#83AEFA", out color2);
    }
    private void OnDisable()
    {
        mes.Clear();
        mes1.Clear();
        foreach(Image im in cabinetBackground)
        {
            im.color = Color.white;
        };
        foreach(Image im in senatBackground)
        {
            im.color = Color.white;

        };
    }
    public void OnChooseButton(bool table1)
    {
        if (table1)
        {
            tables[0].SetActive(true);
            //buttons[0].color = Color.white;
            //titles[0].color = Color.black;
            buttons[0].color =color; //C13A0E
            titles[0].color = Color.white;
            tables[1].SetActive(false);
            buttons[1].color = Color.white;
            titles[1].color = Color.black;
        }
        else
        {
            tables[0].SetActive(false);
            buttons[0].color = Color.white;
            titles[0].color = Color.black;
            tables[1].SetActive(true);
            buttons[1].color = color;
            titles[1].color = Color.white;
        }
    }

    async public override void SetUp()
    {
        base.SetUp();
        OnChooseButton(true);
        foreach (InputField ipf in messages)
        {
            ipf.text = null;
        };
        foreach (InputField ipf in messages1)
        {
            ipf.text = null;

        };
        displayMessage = false;
        displayMessage1 = false;
        mes = new List<string>();
        mes1 = new List<string>();

        Calendar cal = dfi.Calendar;
        int month_ = cal.GetMonth(DateTime.Now);
        string month = month_ + "_" + DateTime.Now.Year;

        HighscoresManager.Highscores allClubsHighscores_monthly = Sort(await Read_AllClubs_Highscores_Monthly(month));
        for (int i = 0; i < 10; i++)
        {
            if (allClubsHighscores_monthly.highscoreEntryList[i] != null)
            {
                names[i].text = allClubsHighscores_monthly.highscoreEntryList[i].time_name;
                if (allClubsHighscores_monthly.highscoreEntryList[i].userId == UserInfoManager.Instance.userInfo.userID && allClubsHighscores_monthly.highscoreEntryList[i].time_name.Contains(UserInfoManager.Instance.userInfo.club.ToString()))
                {
                    messages[i].interactable = true;
                    cabinetBackground[i].color = color2;
                }
                else
                {
                    messages[i].interactable = false;
                }
                await FBDatabase.ReadMessage(allClubsHighscores_monthly.highscoreEntryList[i].userId).ContinueWith(task =>
                {
                    if (task.IsCompleted)
                    {
                        if (task.Result != null)
                            Debug.LogError(task.Result);
                        mes.Add(task.Result);
                    }
                });
            }
        };

        HighscoresManager.Highscores allClubsRanking_monthly = Sort(await Read_AllClubs_Ranking_Monthly(month));
        for (int i = 0; i < 5; i++)
        {
            if (allClubsRanking_monthly.highscoreEntryList[i] != null)
            {
                names1[i].text = allClubsRanking_monthly.highscoreEntryList[i].time_name;
                if (allClubsRanking_monthly.highscoreEntryList[i].userId == UserInfoManager.Instance.userInfo.userID && allClubsRanking_monthly.highscoreEntryList[i].time_name.Contains(UserInfoManager.Instance.userInfo.club.ToString()))
                {
                    messages1[i].interactable = true;
                    senatBackground[i].color = color2;
                }
                else
                {
                    messages1[i].interactable = false;
                }
                await FBDatabase.ReadMessage(allClubsRanking_monthly.highscoreEntryList[i].userId).ContinueWith(task =>
                {
                    if (task.IsCompleted)
                    {
                        if (task.Result != null)
                            Debug.LogError(task.Result);
                        mes1.Add(task.Result);
                    }
                });
            }
        };
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
        if (!displayMessage)
        {
            if (mes.Count > 9)
            {
                displayMessage = true;
                for (int i = 0; i < 10; i++)
                {
                    messages[i].placeholder.GetComponent<Text>().text = mes[i];
                }
            }
        }
        if (!displayMessage1)
        {
            if (mes1.Count > 4)
            {
                displayMessage1 = true;
                for (int i = 0; i < 5; i++)
                {
                    messages1[i].placeholder.GetComponent<Text>().text = mes1[i];

                }
            }
        }

        if (tables[0].activeSelf)
        {
            foreach (InputField ipf in messages)
            {
                if (ipf.interactable)
                {
                    if (ipf.text != "")
                    {
                        if (save != null)
                            save.interactable = true;
                    }
                    else
                    {
                        if (save != null)
                            save.interactable = false;
                    }
                }
            };
        }
        else
        {
            foreach (InputField ipf in messages1)
            {
                if (ipf.interactable)
                {
                    if (ipf.text != "")
                    {
                        if (save != null)
                            save.interactable = true;
                    }
                    else
                    {
                        if (save != null)
                            save.interactable = false;
                    }
                }
            };
        }


    }
    async public void Save()
    {
        if (tables[0].activeSelf)
        {
            foreach (InputField message in messages)
            {
                if (message.interactable)
                {
                    await FBDatabase.WriteMessage(UserInfoManager.Instance.userInfo.userID, message.text).ContinueWith(task =>
                    {
                        if (task.IsCompleted)
                        {
                            Debug.Log(UserInfoManager.Instance.userInfo.userID);
                            Debug.Log(message.text);
                            Debug.Log("Save Done");
                            displayMessage = false;
                        }
                    });
                }
            }
        }
        else
        {
            foreach (InputField message in messages1)
            {
                if (message.interactable)
                {
                    await FBDatabase.WriteMessage(UserInfoManager.Instance.userInfo.userID, message.text).ContinueWith(task =>
                    {
                        if (task.IsCompleted)
                        {
                            Debug.Log(UserInfoManager.Instance.userInfo.userID);
                            Debug.Log(message.text);
                            Debug.Log("Save Done");
                            displayMessage1 = false;
                        }
                    });
                }
            }
        }
        mes.Clear();
        mes1.Clear();
        SetUp();

    }
    async public static Task<string> Read_AllClubs_Highscores_Monthly(string month)
    {
        string jsonString = null;
        await FBDatabase.Read_AllClubs_Highscores_Monthly(month).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                jsonString = task.Result;
            }
        });
        return jsonString;
    }
    async public static Task<string> Read_AllClubs_Ranking_Monthly(string month)
    {
        string jsonString = null;
        await FBDatabase.Read_AllClubs_Ranking_Monthly(month).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                jsonString = task.Result;
            }
        });
        return jsonString;
    }
    //Sort list scores
    private HighscoresManager.Highscores Sort(string scores)
    {
        HighscoresManager.Highscores highscores = JsonUtility.FromJson<HighscoresManager.Highscores>(scores);
        for (int i = 0; i < highscores.highscoreEntryList.Count; i++)
        {
            for (int j = i + 1; j < highscores.highscoreEntryList.Count; j++)
            {
                if (highscores.highscoreEntryList[j].score > highscores.highscoreEntryList[i].score)
                {
                    // Swap
                    HighscoresManager.HighscoreEntry tmp = highscores.highscoreEntryList[i];
                    highscores.highscoreEntryList[i] = highscores.highscoreEntryList[j];
                    highscores.highscoreEntryList[j] = tmp;
                }
            }
        }
        return highscores;
    }
    public void Back()
    {
        ViewsManager.Instance.ChangeView(previousView);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void ShowNotice(Image notice)
    {
        exit.interactable = false;
        notice.gameObject.SetActive(true);
    }
    public void OffNotice(Image notice)
    {
        notice.gameObject.SetActive(false);
        exit.interactable = true;
    }
}
