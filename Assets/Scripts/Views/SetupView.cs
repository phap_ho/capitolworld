﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SetupView : Views
{
    [SerializeField] InputField nickName;
    [SerializeField] Text nameClub;
    private int clubNum;
    [SerializeField] Image exit;
    public void OnPlay()
    {
        UserInfoManager.Instance.userInfo.name = nickName.text;
        UserInfoManager.Instance.userInfo.club = (Club)clubNum;
        UserInfoManager.Instance.SaveData();
        ViewsManager.Instance.ChangeView(ViewType.SelectRoundView);
    }
    public override void SetUp()
    {
        exit.gameObject.SetActive(false);
        if (UserInfoManager.Instance.userInfo != null)
        {
            clubNum = (int)UserInfoManager.Instance.userInfo.club;
            base.SetUp();
            if (UserInfoManager.Instance.userInfo.name != "")
            {
                nickName.text = UserInfoManager.Instance.userInfo.name;
            }
            nameClub.text = ((Club)clubNum).ToString();
            UserInfoManager.Instance.ChangeShirtMaterial(clubNum);
        }
    }
    public void ForwardButton()
    {
        //clubNum = clubNum > 12 ? 0 : clubNum + 1;
        clubNum = clubNum > 0 ? 0 : clubNum + 1;
        //Debug.Log(clubNum);
        nameClub.text = ((Club)clubNum).ToString();
        UserInfoManager.Instance.ChangeShirtMaterial(clubNum);

    }
    public void ReverseButton()
    {
        //clubNum = clubNum < 1 ? 13 : clubNum - 1;
        clubNum = clubNum < 1 ? 1 : clubNum - 1;
        //Debug.Log(clubNum);
        nameClub.text = ((Club)clubNum).ToString();
        UserInfoManager.Instance.ChangeShirtMaterial(clubNum);
    }
    public void LogOut()
    {
        PlayerPrefs.DeleteAll();
        UserInfoManager.Instance.userInfo = null;
        ViewsManager.Instance.LoadSceneByName("Login");
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        if (Input.GetKey(KeyCode.Escape))
        {
            ShowNotice(exit);
        }
    }
    public void ShowNotice(Image notice)
    {
        notice.gameObject.SetActive(true);
    }
    public void OffNotice(Image notice)
    {
        notice.gameObject.SetActive(false);
    }
    public void LoadTheCapinetView()
    {
        TheCapinetView.previousView = ViewType.SetupView;
        ViewsManager.Instance.ChangeView(ViewType.TheCapinetView);
    }
}
