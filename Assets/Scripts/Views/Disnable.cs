﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disnable : MonoBehaviour
{
    public float seconds;
    private IEnumerator DisableAfterSeconds()
    {
        yield return new WaitForSeconds(seconds);
        gameObject.SetActive(false);
    }
    private void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(DisableAfterSeconds());
    }
}
