﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LoginManager : MonoBehaviour
{
    private void Awake()
    {

        if (PlatformManager.inst.platformType != PlatformType.UWP)
        {
#if UNITY_STANDALONE_WIN || UNITY_ANDROID
            Auth.isLoggedin += (ob) =>
            {
                if (ob)
                {
                    ViewsManager.Instance.LoadSceneByName("Setup");
                    //UserInfoManager.Instance.GetDataLocal();
                }
                else
                {
                    ViewsManager.Instance.ChangeView(ViewType.LoginView);
                }
            };
#endif
        }
        else
        {
            if (UserInfoManager.Instance.userInfo != null)
            {
                DatabaseRESTAPI.CheckAccount(UserInfoManager.Instance.userInfo.userID, () =>
                {
                    ViewsManager.Instance.LoadSceneByName("Setup");
                },
                () =>
                {
                    ViewsManager.Instance.ChangeView(ViewType.LoginView);
                });
            }
            else
                ViewsManager.Instance.ChangeView(ViewType.LoginView);
        }
    }
}
