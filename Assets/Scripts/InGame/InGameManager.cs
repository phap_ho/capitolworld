﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public enum IngameState
{
    Ingame,
    IAP,
    EnterKeyport,
    DrinkBeer, //or Eating Bratwurst
    Endgame,
    CatchedBySecurity,
    ChangeGameScene,
    Leaderboard
}
public enum IngameType
{
    WalkingStreet,
    OutsideStadium,
}
public class InGameManager : Singleton<InGameManager>
{
    #region "OB Reference"
    [Header("OB Reference")]
    [SerializeField] Transform bembelKeyPort;
    [SerializeField] Transform beerKeyPort;
    [SerializeField] Transform brawurstKeyPort;
    #endregion

    #region "Setting"
    [Header("Setting")]
    public int timeToPlay;
    public int timeBonus;
    public readonly int rateAttack = 2;
    protected bool notYetDisplayKeyport = true;
    public IngameType IngameType;
    #endregion

    #region "Ingame State"
    [Header("GameState")]
    public float timeNow;
    public float timeAttack;
    public bool canAttack;
    public Action Endgame;
    private IngameState ingameState;
    public IngameState IngameState
    {
        set
        {
            ingameState = value;
            switch (ingameState)
            {
                case IngameState.Ingame:
                    Time.timeScale = 1;
                    ViewsManager.Instance.ChangeView(ViewType.InGameView);
                    break;
                case IngameState.EnterKeyport:
                    Time.timeScale = 0;
                    if (IngameType == IngameType.WalkingStreet)
                    {
                        ViewsManager.Instance.ChangeView(ViewType.BarInteriorView);
                    }
                    else
                    {
                        ViewsManager.Instance.ChangeView(ViewType.InsideStadiumView);
                    };
                    break;
                case IngameState.IAP:
                    Time.timeScale = 0;
                    ViewsManager.Instance.ChangeView(ViewType.IAPView);
                    break;
                case IngameState.DrinkBeer:
                    break;
                case IngameState.Endgame:
                    //if (IngameType == IngameType.WalkingStreet)
                    //    ViewsManager.Instance.LoadSceneByName("OutsideStadium 1");
                    //else
                    //{
                    //    HighscoreTable.timeEnd = true;
                    //    SetupManager.SetUpView(ViewType.LeaderBoardView);
                    //    ViewsManager.Instance.LoadSceneByName("SetUp");
                    //}
                    HighscoreTable.timeEnd = true;
                    SetupManager.SetUpView(ViewType.LeaderBoardView);
                    ViewsManager.Instance.LoadSceneByName("SetUp");
                    break;
                case IngameState.ChangeGameScene:
                    break;
                case IngameState.CatchedBySecurity:
                    break;
                case IngameState.Leaderboard:
                    Time.timeScale = 0;
                    ViewsManager.Instance.ChangeView(ViewType.LeaderBoardView);
                    break;
            };
        }
        get
        {
            return ingameState;
        }
    }
    #endregion

    private void Awake()
    {
        if (IngameType == IngameType.WalkingStreet) ScoreManager.Reset();
        IngameState = IngameState.Ingame;
        Endgame += EndGame;
        m_ShuttingDown = false;
        m_Instance = null;
        if (IngameType == IngameType.OutsideStadium)
        {
            Security.catched += PlayerIsCatched;
        }
    }
    private void Update()
    {
        TimeInGame();
    }
    protected void TimeInGame()
    {
        timeNow += Time.deltaTime;
        if (timeNow >= timeToPlay - 60)
            DisplayKeyport();
        if (timeNow >= timeToPlay)
            Endgame?.Invoke();
        if (Security.securityState != SecurityState.Punispunishment)
        {
            if (Mascot.Instance.mascotState == MascotState.Idle)
                timeAttack = timeAttack < rateAttack ? timeAttack + Time.deltaTime : rateAttack;
            canAttack = timeAttack >= rateAttack;
        }
    }
    protected void DisplayKeyport()
    {
        if (notYetDisplayKeyport)
        {
            notYetDisplayKeyport = false;
            Transform ob;
            if (IngameType == IngameType.WalkingStreet)
            {
                if (UserInfoManager.Instance.userInfo.club == Club.Frankfurt)
                {
                    ob = Instantiate(bembelKeyPort, Mascot.Instance.player.transform.position + 4 * Vector3.right, Quaternion.identity);
                    ob.transform.position = new Vector3(ob.transform.position.x, 1, ob.transform.position.z);
                }
                else
                {
                    ob = Instantiate(beerKeyPort, Mascot.Instance.player.transform.position + 4 * Vector3.right, Quaternion.identity);
                    ob.transform.position = new Vector3(ob.transform.position.x, 0.5f, ob.transform.position.z);
                }
            }
            else
            {
                ob = Instantiate(brawurstKeyPort, Mascot.Instance.player.transform.position + 4 * Vector3.right, Quaternion.identity);
                ob.transform.position = new Vector3(ob.transform.position.x, 0.25f, ob.transform.position.z);
            }
            Waypoint.keyport = ob.gameObject;
        }
    }
    public void EndGame()
    {
        IngameState = IngameState.Endgame;
        Endgame -= EndGame;
    }
    private void PlayerIsCatched(Transform ob)
    {
        IngameState = IngameState.CatchedBySecurity;
    }

    private void OnDestroy()
    {
        m_ShuttingDown = false;
        m_Instance = null;
        if (Security.catched != null)
            Security.catched -= PlayerIsCatched;
    }
}
