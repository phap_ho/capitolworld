﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum Club
{
    RedTeam,
    BlueTeam,
    Berlin,
    Bremen,
    Dortmund,
    Dresden,
    Frankfurt,
    Freiburg,
    Gelsenkirchen,
    Hamburg,
    Hannover,
    Monchengladbach,
    Munchen,
    Nuremberg,
    HamburgPirates,
    Wolfsburg,
}

public class UserInfo
{
    public string userID;
    public string name;
    public int coinsNum;
    public Club club;
}
public class UserInfoManager : Singleton<UserInfoManager>
{
    public UserInfo userInfo = new UserInfo();
    [SerializeField] List<Texture> shirtTex;
    [SerializeField] Material shirtMat;
    private void Awake()
    {
        GetDataLocal();
        if (userInfo != null)
            GetData(userInfo.userID);
        //userInfo.coinsNum = 0;
    }
    public void SaveDataLocal()
    {
        string json = JsonUtility.ToJson(userInfo);
        PlayerPrefs.SetString("UserInfo", json);
    }
    public void SaveData()
    {
        string json = JsonUtility.ToJson(userInfo);
        PlayerPrefs.SetString("UserInfo", json);
        if (PlatformManager.inst.platformType != PlatformType.UWP)
        {
#if UNITY_STANDALONE_WIN || UNITY_ANDROID
            FBDatabase.SaveUserInfo(userInfo.userID, json).ContinueWith(ob =>
            {
            });
#endif
        }
        else
        {
            DatabaseRESTAPI.SaveUserInfo(userInfo.userID, json, null);
        }
    }
    public void GetDataLocal()
    {
        string json = PlayerPrefs.GetString("UserInfo");
        userInfo = JsonUtility.FromJson<UserInfo>(json);
        Debug.Log(json);
    }
    public void GetData(string userID)
    {
        if(PlatformManager.inst.platformType != PlatformType.UWP)
        {
#if UNITY_STANDALONE_WIN || UNITY_ANDROID
            FBDatabase.ReadUserInfoOnce(userID).ContinueWith(ob =>
            {
                //Debug.LogError(ob.Result);
                userInfo = JsonUtility.FromJson<UserInfo>(ob.Result);
            });
#endif
        }
        else
        {
            DatabaseRESTAPI.ReadUserInfoOnce(userID, (userInfo)=> {
                this.userInfo = userInfo;
            });
        }

    }
    public void ChangeShirtMaterial(int shirtIndex)
    {
        Debug.LogError(shirtIndex);
        shirtMat.mainTexture = shirtTex[shirtIndex];
    }
    private void Update()
    {
        //Debug.LogError(userInfo.name);
    }
}
