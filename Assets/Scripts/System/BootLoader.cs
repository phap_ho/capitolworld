﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BootLoader : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this);
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("Quality", 3), true);
    }
    private void Start()
    {
        //ViewsManager.Instance.ChangeView(ViewType.DowloadUpdateView);
        ViewsManager.Instance.LoadSceneByName("Login");
    }
}
