﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrinkingBeer : MonoBehaviour
{
    [SerializeField] GameObject beerGlass;
    [SerializeField] GameObject bembel;
    private void OnEnable()
    {
        bembel.SetActive(UserInfoManager.Instance.userInfo.club == Club.Frankfurt);
        beerGlass.SetActive(UserInfoManager.Instance.userInfo.club != Club.Frankfurt);
    }
}
