﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TrophySpawnManager : MonoBehaviour
{
    [SerializeField] List<Transform> pointSpawns;
    [SerializeField] List<Trophy> trophies;
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        for (int i = 0; i < 3; i++)
        {
            int point = Random.Range(0, pointSpawns.Count);
            int trophy = Random.Range(0, trophies.Count);
            Instantiate(trophies[trophy], pointSpawns[point].transform);
            pointSpawns.RemoveAt(point);
            trophies.RemoveAt(trophy);
        }
    }

}
