﻿using System;
using UnityEngine;
#if UNITY_STANDALONE_WIN || UNITY_ANDROID
using Firebase.Auth;
# endif


public class Auth : MonoBehaviour
{
    public static Action<bool> isLoggedin;
    public static Action<bool, string> login;
    public static Action<bool> signUp;
    public static Action<bool> resetPassWord;

#if UNITY_STANDALONE_WIN || UNITY_ANDROID
    private static FirebaseAuth auth;
    public void Awake()
    {
        //string version = null;
        //await FirebaseDatabse.GetVersion().ContinueWith(task=> {
        //    if (task.IsCompleted)
        //        version = task.Result;
        //});
        //if (version != "\"0.0.0\"")
        //{
        //    Debug.Log(version);
        //    Debug.LogError("Old version");
        //}
        auth = FirebaseAuth.DefaultInstance;
    }
    async public void Start()
    {
        if (PlatformManager.inst.platformType == PlatformType.Windows)
        {
            if (UserInfoManager.Instance.userInfo != null && UserInfoManager.Instance.userInfo.userID != "")
            {
                if (await FBDatabase.CheckAccount(UserInfoManager.Instance.userInfo.userID))
                {
                    isLoggedin?.Invoke(true);
                }
                else
                    isLoggedin?.Invoke(false);
            }
            else
            {
                isLoggedin?.Invoke(false);
            }
        }
    }

    public static void SignUp(string email, string password)
    {
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                signUp?.Invoke(false);
                return;
            }
            if (task.IsFaulted)
            {
                signUp?.Invoke(false);
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            // Firebase user has been created.
            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
            signUp?.Invoke(true);

        });
    }

    public static void SignIn(string email, string password)
    {

        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {

                login?.Invoke(false, null);
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {

                login?.Invoke(false, null);
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }
            FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
            //Debug.LogError("Login invoke action");
            login?.Invoke(true, newUser.UserId);
        });
    }
    public static void ResetPassword(string email)
    {
        auth.SendPasswordResetEmailAsync(email).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                resetPassWord?.Invoke(false);
                Debug.LogError("SendPasswordResetEmailAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                resetPassWord?.Invoke(false);
                Debug.LogError("SendPasswordResetEmailAsync encountered an error: " + task.Exception);
                return;
            }
            resetPassWord?.Invoke(true);
            Debug.Log("Password reset email sent successfully.");
        });
    }
#endif
}
